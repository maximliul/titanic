import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def is_gender(value):
    if 'Mr.' in value:
        return 'Mr.'
    elif 'Mrs.' in value:
        return 'Mrs.'
    elif 'Miss.' in value:
        return 'Miss.'
    else:
        return '-'
        
def get_filled():
    df = get_titatic_dataframe()
    df['Gender'] = df['Name'].apply(lambda x: is_gender(x))
    df_selected_gender = df.loc[df['Gender'].isin(["Mr.", "Mrs.", "Miss."])].copy()
    df_gr_gender = df_selected_gender.groupby('Gender', as_index=False)['Age'].median()
    df_gr_gender['Age'] = df_gr_gender['Age'].apply(lambda x: round(x))
    df_gr_nan = df_selected_gender['Age'].isnull().groupby(df_selected_gender['Gender']).sum().astype(int).reset_index(name='nan')
    df_merged = df_gr_nan.merge(df_gr_gender, on='Gender')
    sortMap = {'Mr.': 1, 'Mrs.': 2, 'Miss.': 3}
    df_merged["new"] = df_merged['Gender'].map(sortMap)
    df_merged.sort_values('new', inplace=True)
    return list(df_merged[['Gender', 'nan', 'Age']].itertuples(index=False, name=None))
